from flask import Flask
app = Flask(__name__)

from flask_bootstrap import Bootstrap
Bootstrap(app)
app.config['BOOTSTRAP_SERVE_LOCAL'] = True

