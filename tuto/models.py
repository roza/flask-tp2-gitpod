import yaml, os.path

Books = yaml.safe_load(
open(
  os.path.join(
    os.path.dirname(__file__),
    "data.yml")))

def get_sample():
    id = 0
    for book in Books[0:20]:
        book['id'] = id
        id += 1
    return Books[0:20]

def get_book_detail(bookid):
    return Books[bookid]